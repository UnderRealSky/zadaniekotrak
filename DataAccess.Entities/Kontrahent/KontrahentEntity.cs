﻿using DataAccess.Entities.Base;

namespace DataAccess.Entities.Kontrahent
{
    public class KontrahentEntity : BaseEntity
    {

        public string FirstName { get; set; }
        public string LastName { get; set; }

    }
}