﻿using Newtonsoft.Json;
using System;

namespace DataAccess.Entities.Base
{
    public class BaseEntity
    {
        [JsonProperty("$id")]
        public int IntenalId { get; set;}
        public Guid Id { get; set; }
        public DateTime CreationTime { get; set; }
    }
}