﻿using System;
using AppDomain;
using AppDomain.Interfaces;
using Application.Definitions.Commands;
using Application.Definitions.Dtos;

namespace Application.Application.CommandHandlers
{
    public class UpdateKontrahentCommandHandler : ICommandHandler<EditKontrahentCommand, EditKontrahentResponseDto>
    {
        private readonly IUpdateAggregate<Kontrahent> _updater;

        public UpdateKontrahentCommandHandler(IUpdateAggregate<Kontrahent> updater)
        {
            _updater = updater;
        }

        public EditKontrahentResponseDto Handle(EditKontrahentCommand command)
        {
            var results  = _updater.Update(command.Id, new Kontrahent() {LastName = command.LastName, FirstName = command.FirstName});

            return new EditKontrahentResponseDto(){Id = results.Id, FirstName = results.FirstName, LastName = results.LastName};
        }
    }
}