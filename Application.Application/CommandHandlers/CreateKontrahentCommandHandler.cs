﻿using AppDomain;
using AppDomain.Interfaces;
using Application.Definitions.Commands;
using Application.Definitions.Dtos;

namespace Application.Application.CommandHandlers
{
    public class CreateKontrahentCommandHandler: ICommandHandler<CreateKontrahentCommand, CreateKontrahentResponseDto>
    {
        private readonly ICreateAggregate<Kontrahent> _kontrahentCreator;

        public CreateKontrahentCommandHandler(ICreateAggregate<Kontrahent> kontrahentCreator)
        {
            _kontrahentCreator = kontrahentCreator;
        }
        public CreateKontrahentResponseDto Handle(CreateKontrahentCommand command)
        {
            var results = _kontrahentCreator.CreateNew(
                new Kontrahent()
                {
                    FirstName = command.FirstName,
                    LastName = command.LastName
                }
            );

            var dto = ConvertResults(results);
            return dto;
        }

        private static CreateKontrahentResponseDto ConvertResults(Kontrahent results)
        {
            var dto = new CreateKontrahentResponseDto
            {
                LastName = results.LastName,
                FirstName = results.FirstName,
                Id = results.Id
            };
            return dto;
        }
    }
}