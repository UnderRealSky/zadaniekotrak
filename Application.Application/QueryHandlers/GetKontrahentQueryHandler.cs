﻿using System;
using System.Linq.Expressions;
using AppDomain;
using AppDomain.Interfaces;
using Application.Definitions.Dtos;
using Application.Definitions.Queries;

namespace Application.Application.QueryHandlers
{
    public class GetKontrahentQueryHandler : IQueryHandler<GetKontrahentDetailsQuery, GetKontrahentDetailsResponseDto>
    {
        private readonly ILoadAggregate<Kontrahent> _kontrahentLoader;

        public GetKontrahentQueryHandler(ILoadAggregate<Kontrahent> kontrahentLoader)
        {
            _kontrahentLoader = kontrahentLoader;
        }

        public GetKontrahentDetailsResponseDto Handle(GetKontrahentDetailsQuery command)
        {
            Func<Kontrahent, bool> fNameCheck = (kontrahent => kontrahent.FirstName == command.FirstName);
            
            //TODO: jak zrobić to bardziej dynamiczne. 
            // var fNameCheck = string.IsNullOrEmpty(command.FirstName)
            //    ? (Func<Kontrahent, bool>)(kontrahent => kontrahent.FirstName == command.FirstName) : (kontrahent => true);
            //var lNameCheck = string.IsNullOrEmpty(command.LastName)
            //    ? (Func<Kontrahent, bool>)(kontrahent => kontrahent.LastName == command.LastName) : (kontrahent => true);

            //Func<Kontrahent, bool> idCheck = command.Id != null  ? (Func<Kontrahent, bool>) (kontrahent => kontrahent.Id == command.Id) : (kontrahent => true);

            var results =
                _kontrahentLoader.Load(kontrahent => fNameCheck(kontrahent)
                                                     //lNameCheck(kontrahent) ||
                                                     //idCheck(kontrahent)
                                                     );

            return new GetKontrahentDetailsResponseDto(){Id = results.Id, FirstName = results.FirstName, LastName = results.LastName, Created = results.CreationDate};
        }
    }
}