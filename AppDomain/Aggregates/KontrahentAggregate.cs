﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AppDomain.Interfaces;
using AppDomain.Repositories;

namespace AppDomain.Aggregates
{
    public class KontrahentAggregate : ILoadAggregate<Kontrahent>, IPersistAggregate<Kontrahent>, ICreateAggregate<Kontrahent>, IUpdateAggregate<Kontrahent>
    {
        private Kontrahent _kontrahent;
        private List<Kontrahent> _kontrahents;

        private readonly IRepository<Kontrahent> _kontrahentRepository;

        public KontrahentAggregate(IRepository<Kontrahent> kontrahentRepository)
        {
            _kontrahentRepository = kontrahentRepository ?? throw new ArgumentNullException(nameof(kontrahentRepository));

        }

        public void CreateNewKontrahent(string firstName, string lastName)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new ArgumentException("first name cannot be empty", nameof(firstName));
            }

            if (string.IsNullOrEmpty(lastName))
            {
                throw new ArgumentException("last name cannot be empty", nameof(lastName));
            }

            _kontrahent = new Kontrahent() { FirstName = firstName, LastName = lastName };
            Persist(_kontrahent);
        }


        public IEnumerable<Kontrahent> LoadAll()
        {
            _kontrahents = _kontrahentRepository.GetAll().ToList();
            return _kontrahents;
        }

        public Kontrahent Load(Func<Kontrahent, bool> predicate)
        {
            _kontrahent = _kontrahentRepository.GetDetail(predicate);
            return _kontrahent;
        }

        public void Persist(Kontrahent aggregate)
        {
            _kontrahentRepository.Add(aggregate);
        }

        public Kontrahent CreateNew(Kontrahent entity)
        {
            try
            {
                var id = Guid.NewGuid();
                entity.Id = id;
                _kontrahentRepository.Add(entity);
                _kontrahent = entity;
                return _kontrahent;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            
        }

        public Kontrahent Update(Guid kontrahentId, Kontrahent newData)
        {
            var currentState = Load(kontrahent => kontrahent.Id == kontrahentId);


            if (currentState != null && currentState.Id == kontrahentId)
            {
                if (string.IsNullOrEmpty(newData.FirstName))
                {
                    currentState.FirstName = newData.FirstName;
                }

                if (string.IsNullOrEmpty(newData.LastName))
                {
                    currentState.FirstName = newData.LastName;
                }
                _kontrahent = currentState;
                _kontrahentRepository.Update(kontrahentId, newData);
            }
            return _kontrahent;
        }

        public override string ToString()
        {
            if (_kontrahent != null)
            {
                return $"Kontrahent: Imię: {_kontrahent.FirstName}, Nazwisko: {_kontrahent.LastName}";
            }
            return base.ToString();
        }
    }
}