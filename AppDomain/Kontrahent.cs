﻿using System;

namespace AppDomain
{
    public class Kontrahent
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid Id { get; set; }
        public DateTime CreationDate { get; set; }
    }
}