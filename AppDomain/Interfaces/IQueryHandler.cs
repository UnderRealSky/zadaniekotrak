﻿namespace AppDomain.Interfaces
{
    public interface IQueryHandler<in TCommand, out TResponse>
    {
        TResponse Handle(TCommand command);
    }
}