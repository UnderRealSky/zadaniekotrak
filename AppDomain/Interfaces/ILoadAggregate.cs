﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace AppDomain.Interfaces
{
    public interface ILoadAggregate<out T>
    {
        IEnumerable<T> LoadAll();
        T Load(Func<T, bool> predicate);
    }
}