﻿namespace AppDomain.Interfaces
{
    public interface ICommandHandler<in TCommand, out TResponse>
    {
        TResponse Handle(TCommand command);
    }
}