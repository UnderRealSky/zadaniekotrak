﻿using System;

namespace AppDomain.Interfaces
{
    public interface IUpdateAggregate<T>
    {
        T Update(Guid kontrahentId, T newData);
    }
}