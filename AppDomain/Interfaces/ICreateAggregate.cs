﻿using System;

namespace AppDomain.Interfaces
{
    public interface ICreateAggregate<T>
    {
        T CreateNew(T entity);
    }
}