﻿namespace AppDomain.Interfaces
{
    public interface IPersistAggregate<in T> where T : new()
    {
        void Persist(T aggregate);
    }
}