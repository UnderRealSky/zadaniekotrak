﻿namespace Application.Definitions.Commands
{
    public class CreateKontrahentCommand
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BankNumber { get; set; }
        public string Email { get; set; }
    }
}