﻿using System;

namespace Application.Definitions.Dtos
{
    public class CreateKontrahentResponseDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BankNumber { get; set; }
        public string Email { get; set; }
        public Guid Id { get; set; }
    }
}