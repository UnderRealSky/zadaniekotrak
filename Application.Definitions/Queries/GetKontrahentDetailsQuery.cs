﻿using System;

namespace Application.Definitions.Queries
{
    public class GetKontrahentDetailsQuery
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public Guid? Id { get; set; }
    }
}