﻿using DataAccess.JsonDatabase;
using System;
using AppDomain;
using AppDomain.Aggregates;
using AppDomain.Interfaces;
using AppDomain.Repositories;
using Application.Definitions.Commands;
using Application.Definitions.Dtos;
using DataAccess.Entities.Kontrahent;
using DataAccess.JsonDatabase.Factories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Application.Application.CommandHandlers;
using Application.Application.QueryHandlers;
using Application.Definitions.Queries;

namespace ZadanieKotrak
{
    class Program
    {
        static void Main(string[] args)
        {

            // szybkie DI
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<IJsonRepositoryConnectionFactory<KontrahentEntity>, JsonRepositoryConnectionFactory<KontrahentEntity>>()
                .AddScoped<IRepository<Kontrahent>, KontrahentRepository>()
                .AddScoped<ILoadAggregate<Kontrahent>, KontrahentAggregate>()
                .AddScoped<IPersistAggregate<Kontrahent>, KontrahentAggregate>()
                .AddScoped<ICreateAggregate<Kontrahent>, KontrahentAggregate>()
                .AddScoped<IUpdateAggregate<Kontrahent>, KontrahentAggregate>()
                .AddScoped<ICommandHandler<CreateKontrahentCommand, CreateKontrahentResponseDto>, CreateKontrahentCommandHandler>()
                .AddScoped<ICommandHandler<EditKontrahentCommand, EditKontrahentResponseDto>, UpdateKontrahentCommandHandler>()
                .AddScoped<IQueryHandler<GetKontrahentDetailsQuery, GetKontrahentDetailsResponseDto>, GetKontrahentQueryHandler>()
                .BuildServiceProvider();

            Console.WriteLine("Hello World!");
           
            // Pobieranie instancji handlerów
            // Tworzenie Kontrahenta
            var handler = serviceProvider.GetService<ICommandHandler<CreateKontrahentCommand, CreateKontrahentResponseDto>>();

            var dummyFirstName = $"{DateTime.Now.Ticks.ToString()}rek";

            var res = handler.Handle(new CreateKontrahentCommand()
            {
                FirstName = dummyFirstName,
                LastName = "Nieznajomy",
                Email = "tego@nie.zapisze.com"
            });
            // Wyszukiwanie kontrahenta po imieniu
            var query = serviceProvider.GetService<IQueryHandler<GetKontrahentDetailsQuery, GetKontrahentDetailsResponseDto>>();
            var user = query.Handle(new GetKontrahentDetailsQuery() {FirstName = dummyFirstName });

            Console.WriteLine($"Kontrahent: {user.FirstName} {user.LastName}, dodano: {user.Created.ToShortDateString()}");


            // Aktualizacja Kontrahenta
            var updateCommandHandler = serviceProvider.GetService<ICommandHandler<EditKontrahentCommand, EditKontrahentResponseDto>>();

            updateCommandHandler.Handle(new EditKontrahentCommand() {LastName = "YYYYYY", Id = user.Id});

            // Ponowne Wyszukiwanie kontrahenta po imieniu
            var userAgain = query.Handle(new GetKontrahentDetailsQuery() { FirstName = dummyFirstName });
            Console.WriteLine($"Kontrahent: {userAgain.FirstName} {userAgain.LastName}, dodano: {userAgain.Created.ToShortDateString()}");



            Console
                .ReadKey();
        }
    }
}
