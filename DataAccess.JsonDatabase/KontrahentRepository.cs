﻿
using System;
using System.Collections.Generic;
using System.Linq;
using AppDomain;
using AppDomain.Repositories;
using DataAccess.Entities.Kontrahent;
using DataAccess.JsonDatabase.Factories;
using MiniBiggy;

namespace DataAccess.JsonDatabase
{
    public class KontrahentRepository : IRepository<Kontrahent>
    {
        // TODO do fabryki abstrakcyjnej to coś.
        // może nie działać z .net core 
        private readonly PersistentList<KontrahentEntity> _kontrahentRepository;



        public KontrahentRepository(IJsonRepositoryConnectionFactory<KontrahentEntity> kontrahentConnectionFactory)
        {

            _kontrahentRepository = kontrahentConnectionFactory.CreateConnection();
        }

        public void Add(Kontrahent entity)
        {
            _kontrahentRepository.Add(ConvertToDbEntity(entity));
            _kontrahentRepository.Save();
        }

        public void Delete(Kontrahent entity)
        {
            var requested = _kontrahentRepository.FirstOrDefault(e => e.FirstName == entity.FirstName && e.LastName == entity.LastName && e.Id == entity.Id);
            if (requested != null)
            {
                _kontrahentRepository.Remove(requested);
                _kontrahentRepository.Save();
            }
        }

        public void Update(Guid dd, Kontrahent entity)
        {
            var kontahentEntity = _kontrahentRepository.SingleOrDefault(kontr => kontr.Id == dd);
            //To nie powinno tak być. mapper potrzebny
            if(!string.IsNullOrEmpty(entity.FirstName))
                if (kontahentEntity != null) kontahentEntity.FirstName = entity.FirstName;
            if (!string.IsNullOrEmpty(entity.LastName))
                if (kontahentEntity != null) kontahentEntity.LastName = entity.LastName;

            _kontrahentRepository.Save();
            
        }

        public IEnumerable<Kontrahent> GetAll(Func<Kontrahent, bool> predicate = null)
        {
            var allkontrs = _kontrahentRepository.Select(ConvertToKontrahent).ToList<Kontrahent>();
            return allkontrs;
        }

        public Kontrahent GetDetail(Func<Kontrahent, bool> predicate)
        {
            var detailed = _kontrahentRepository.Where(x => predicate(ConvertToKontrahent(x))).Select(ConvertToKontrahent).ToList();
            var totalNum = detailed.Count;
            if (totalNum > 1)
                throw new Exception("Too many results for the query");
            return detailed.FirstOrDefault();
        }


        // kowertery najlepiej do osobnego projektu
        private static KontrahentEntity ConvertToDbEntity(Kontrahent kontrahent)
        {
            return new KontrahentEntity()
            {
                CreationTime = DateTime.Now, FirstName = kontrahent.FirstName, LastName = kontrahent.LastName, Id = kontrahent.Id
            };
        }

        private static Kontrahent ConvertToKontrahent(KontrahentEntity entity)
        {
            return new Kontrahent()
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Id = entity.Id,
               CreationDate = entity.CreationTime
            };
        }

    }
}