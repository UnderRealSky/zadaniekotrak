﻿using MiniBiggy;

namespace DataAccess.JsonDatabase.Factories
{
    public interface IJsonRepositoryConnectionFactory<TEntity> where TEntity : new()
    {
        PersistentList<TEntity> CreateConnection();
    }
}