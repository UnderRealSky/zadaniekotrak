﻿using MiniBiggy;
using MiniBiggy.SaveStrategies;

namespace DataAccess.JsonDatabase.Factories
{
    public class JsonRepositoryConnectionFactory<TEntity> : IJsonRepositoryConnectionFactory<TEntity> where TEntity: new()
    {


        public PersistentList<TEntity> CreateConnection()
        {
            var fileName = $"{typeof(TEntity).Name.ToLower()}.data";
            var persistenceList = Create.ListOf<TEntity>().SavingAt(fileName).UsingPrettyJsonSerializer().SavingWhenRequested();
            persistenceList.Save();
            return persistenceList;
        }
    }
}